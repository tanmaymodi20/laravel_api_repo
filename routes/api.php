<?php

use Illuminate\Http\Request;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// // public routes
// Route::get('/students',[StudentController::class,'index']);

// Route::get('/show/{id}',[StudentController::class,'show']);  

// Route::post('/insert',[StudentController::class,'insert']);

// Route::put('/update/{id}',[StudentController::class,'update']);

// Route::delete('/delete/{id}',[StudentController::class,'delete']);

// Route::get('/search/city/{city}',[StudentController::class,'searchcity']);

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

//pagination url and also public routes 
Route::get('/pagination',[StudentController::class,'pagination']);


Route::post('/register',[UserController::class,'register']);

Route::post('/login',[UserController::class,'login']);


// // protected routes using auth:sanctum

// for group route middlware to access students data and also via register user token
Route::middleware(['auth:sanctum'])->group(function() {
    Route::get('/students',[StudentController::class,'index']);

    Route::get('/show/{id}',[StudentController::class,'show']);  

    Route::post('/insert',[StudentController::class,'insert']);

    Route::put('/update/{id}',[StudentController::class,'update']);

    Route::delete('/delete/{id}',[StudentController::class,'delete']);

    Route::get('/students/search/{city}',[StudentController::class,'searchcity']);
    
    Route::post('/logout',[UserController::class,'logout']);

    Route::get('/search',[StudentController::Class,'search']);

    Route::get('/sorting',[StudentController::Class,'sorting']);

});


//  for single route middleware to access students data and also via register user token
// Route::middleware('auth:sanctum')->get('/students',[StudentController::class,'index']);

