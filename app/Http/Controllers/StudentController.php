<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class StudentController extends Controller
{
    // Show all students data
    public function index()
    {
      return Student::all();
    }

    // show particular student data
    public function show($id)
    {
       return Student::find($id);
    }

    // Insert Student Data
    public function insert(Request $request)
    {
       $request->validate([
           'name' => 'required',
           'city' => 'required',
           'fees' => 'required|min:4',
       
       ]);
   
       $student = New Student;
       $student->name = $request->name;
       $student->city = $request->city;
       $student->fees = $request->fees;
       $student->save();

    //   $student = Student::create($request->all());
      return response()->json([
        'name'    => $student,
        'message' => 'user data create successfully',
        'status'  => 201,
      ]);

    }

    // update student data 
    public function update(Request $request,$id)
        {

            $request->validate([
                'name' => 'required',
                'city' => 'required',
                'fees' => 'required|min:4 ',
            
            ]);

            $std = Student::find($id);
            $std->name = $request->name;
            $std->city = $request->city;
            $std->fees = $request->fees;
            $std->save();        
            return response()->json([
                'message' => 'your data updated',
                'user'    =>  $std,
            ],200);
        }


        //delete specific student
        public function delete(Request $request,$id)
        {
            $std = Student::find($id);
            $delete = $std->delete($id);

            return response([
                'message' => 'your data deleted',
                'user'    =>  $delete,
            ],200);
        }


        // search a city in url
    public function searchcity($city)
        {
                $stdcity =  Student::where('city','=',$city)->get();
               return response()->json([
                   'data'    => $stdcity,
                   'message' => 'search work successfully',
               ]);
        }


        // search by name
    public function search(Request $request)
        {
            $search = $request->search ?? "";
            if($search)
            {
                $data = Student::where('name','like','%'.$request->search.'%')->get();
                return response()->json([
                    'data'    => $data,
                    'message' => 'search done',

                ],200);
            }

            else
            {
              $all = Student::all();
                return response()->json([
                    'message' => 'all data successfully',
                    'data'    =>  $all,
                ]);
            }
        }


        // sorting

        public function sorting(Request $request)
            {
                if($request->sortBy && in_array($request->sortBy,['id','created_at']))
                {
                    $sortBy = $request->sortBy;  
                }
                else
                {
                    $sortBy = 'id';
                }


                if($request->sortOrder && in_array($request->sortOrder,['asc','desc']))
                {
                    $sortOrder = $request->sortOrder;

                }
                else
                {
                    $sortOrder = 'desc';
                }  
                
                $data = Student::orderBy($sortBy,$sortOrder)->get();
                return response()->json([
                    'data'     => $data,
                    'message'  => 'sorting success',
                ],200);

            }
            // pagination
            public function pagination(Request $request)
            {
                $data = Student::get();

                $total=count($data);
                $per_page = 5;
                $current_page = $request->input("page") ?? 1;
        
                $starting_point = ($current_page * $per_page) - $per_page;
        
          
                
                $array = $data->toArray();
                $array = array_slice($array, $starting_point, $per_page, true);
        
                $array = new Paginator($array, $total, $per_page, $current_page, [
                    'path' => $request->url(),
                    'query' => $request->query(),
                ]);

                return response()->json([
                    'data'       =>  $array,
                    'message'    =>  'Pagination done successfully',
                ]);
            }


          
}
