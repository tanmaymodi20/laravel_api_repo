<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash; 
use App\Models\Student;

class UserController extends Controller
{
    public function register(Request $request)
    {
        // validate request
        $request->validate([
            'name'     => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required|confirmed',

        ]);
        // insert user data
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        $token = $user->createToken('mytoken')->plainTextToken;
        return response([
            'user'    =>   $user,
            'token'   =>   $token,
            'message' =>  'user register successfull',
        ],200);   

    }

    public function logout()
        {
            // for logout 
        auth()->user()->tokens()->delete(); 
        return response([
              'message'  => 'successfully logout '
            ]);
        }
    public function login(Request $request)
        {
            // validation for login
         $request->validate([
              'email'    =>   'required|email',
              'password' =>   'required',
            ]);

        $user = User::where('email','=',$request->email)->first();
             if(!$user ||  !Hash::check($request->password,$user->password))
             {
                 return response([
             'message'    =>  'provided credentials are incorrect'
                 ],401);
             }

              // token generate 
        $token = $user->createToken('mytoken')->plainTextToken;
             return response([
                 'user'     =>     $user,
                 'token'    =>     $token ],200);  
        }
}
